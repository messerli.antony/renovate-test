FROM alpine:3.11

# renovate: datasource=pypi depName=ansible
ARG ANSIBLE_VERSION="6.0.0"

RUN apk --no-cache add curl
